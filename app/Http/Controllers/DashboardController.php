<?php

namespace App\Http\Controllers;

use App\Transaksi;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function show($month){
        $monthYear = explode('-', $month);
        $bulan = $monthYear[1];
        $tahun = $monthYear[0];

        $response = [];
        $riders = User::query()
            ->where('kategori_hub_id', Auth::user()->kategori_hub_id)
            ->whereHas('roles', function ($q){
                return $q->where('name', 'RIDER');
            })->get();
        foreach ($riders as $rider){
            $gajis = Transaksi::query()
                ->where('rider_id', $rider->id)
                ->whereMonth('tanggal', '=', $bulan)
                ->whereYear('tanggal', '=', $tahun)
                ->get();
            $series = [];
            foreach ($gajis as $gaji){
                $gaji_total = array($gaji->tanggal, (int)$gaji->gaji_total);
                array_push($series, $gaji_total);
            }
            $row['name'] = $rider->nama;
            $row['data'] = $series;
            array_push($response, $row);
        }
        return response()->json($response);
    }

    public function pie($month){
        $monthYear = explode('-', $month);
        $bulan = $monthYear[1];
        $tahun = $monthYear[0];

        $response = [];
        $riders = User::query()
            ->where('kategori_hub_id', Auth::user()->kategori_hub_id)
            ->whereHas('roles', function ($q){
                return $q->where('name', 'RIDER');
            })->get();
        foreach ($riders as $rider){
            $gajis = Transaksi::query()
                ->where('rider_id', $rider->id)
                ->whereMonth('tanggal', '=', $bulan)
                ->whereYear('tanggal', '=', $tahun)
                ->sum('gaji_total');
            $row['name'] = $rider->nama;
            $row['y'] = (int)$gajis;
            array_push($response, $row);
        }
        return response()->json($response);
    }

    public function show_rider($month){
        $monthYear = explode('-', $month);
        $bulan = $monthYear[1];
        $tahun = $monthYear[0];

        $response = [];
        $riders = User::query()
            ->where('id', Auth::user()->id)->get();
        foreach ($riders as $rider){
            $gajis = Transaksi::query()
                ->where('rider_id', $rider->id)
                ->whereMonth('tanggal', '=', $bulan)
                ->whereYear('tanggal', '=', $tahun)
                ->get();
            $series = [];
            foreach ($gajis as $gaji){
                $gaji_total = array($gaji->tanggal, (int)$gaji->ocd);
                array_push($series, $gaji_total);
            }
            $row['name'] = $rider->nama;
            $row['data'] = $series;
            array_push($response, $row);
        }
        return response()->json($response);
    }

    public function pie_rider($month){
        $monthYear = explode('-', $month);
        $bulan = $monthYear[1];
        $tahun = $monthYear[0];

        $response = [];
        $riders = User::query()
            ->where('id', Auth::user()->id)
            ->get();
        foreach ($riders as $rider){
            $gajis = Transaksi::query()
                ->where('rider_id', $rider->id)
                ->whereMonth('tanggal', '=', $bulan)
                ->whereYear('tanggal', '=', $tahun)
                ->sum('ocd');
            $row['name'] = $rider->nama;
            $row['y'] = (int)$gajis;
            array_push($response, $row);
        }
        return response()->json($response);
    }
}
