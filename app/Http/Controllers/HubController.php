<?php

namespace App\Http\Controllers;

use App\KategoriHub;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HubController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hubs = KategoriHub::all();
        return view('hub.index', ['hubs'=>$hubs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->id;
        $gudang = $request->gudang;
        $wilayah = $request->wilayah;
        $kabkot = $request->kabkot;
        $kecamatan = $request->kecamatan;
        $umk = $request->umk;

        try {
            DB::beginTransaction();
            if ($id == null){
                $HUBcreate = new KategoriHub();
            }else{
                $HUBcreate = KategoriHub::find($id);
            }
            $HUBcreate->wilayah = $wilayah;
            $HUBcreate->gudang = $gudang;
            $HUBcreate->kabkot = $kabkot;
            $HUBcreate->kecamatan = $kecamatan;
            $HUBcreate->umk = $umk;
            $HUBcreate->save();

            DB::commit();
        }catch (\Exception $exception){
            return response()->json(['msg'=>'error simpan data', 'code'=>'0']);
        }

        return response()->json(['msg'=>'berhasil', 'code'=>'1']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $hub = KategoriHub::findOrFail($id);
        return response()->json(['data'=>$hub]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            DB::beginTransaction();
            $hub = KategoriHub::findOrFail($request->id);
            $hub->delete();

            DB::commit();
        }catch (\Exception $exception){
            return response()->json(['msg'=>'error simpan data', 'code'=>'0']);
        }

        return response()->json(['msg'=>'berhasil', 'code'=>'1']);
    }
}
