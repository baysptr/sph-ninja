<?php

namespace App\Http\Controllers;

use App\KategoriHub;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class KategoriHubController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = KategoriHub::all();

        return response()->json([
            'status' => false,
            'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
            'data' => $data
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'gudang' => 'required',
            'wilayah' => 'required',
        ];

        $ruleMessages = [
            'gudang.required' => 'Nama Gudang harus diisi',
            'wilayah.required' => 'Nama Wilayah harus diisi',
        ];

        $validator = Validator::make($request->all(), $rules, $ruleMessages);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'message' => $validator->errors()->all(),
            ]);
        }

        DB::beginTransaction();

        try {
            $kh = new KategoriHub();
            $kh->gudang = $request->gudang;
            $kh->wilayah = $request->wilayah;
            $kh->save();
            DB::commit();

            return response()->json([
                'status' => true,
                'code' => Response::HTTP_OK,
                'data' => 'Kategori HUB berhasil ditambahkan'
            ]);

        }catch (\Exception $e){
            DB::rollback();

            return response()->json([
                'status' => false,
                'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'message' => 'Kategori HUB gagal ditambahkan'
            ]);

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = KategoriHub::query()->findOrFail($id);

        return response()->json([
            'status' => false,
            'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'gudang' => 'required',
            'wilayah' => 'required',
        ];

        $ruleMessages = [
            'gudang.required' => 'Nama Gudang harus diisi',
            'wilayah.required' => 'Nama Wilayah harus diisi',
        ];

        $validator = Validator::make($request->all(), $rules, $ruleMessages);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'message' => $validator->errors()->all(),
            ]);
        }

        DB::beginTransaction();

        try {
            $kh = KategoriHub::query()->findOrFail($id);
            $kh->gudang = $request->gudang;
            $kh->wilayah = $request->wilayah;
            $kh->save();
            DB::commit();

            return response()->json([
                'status' => true,
                'code' => Response::HTTP_OK,
                'data' => 'Kategori HUB berhasil diubah'
            ]);

        }catch (\Exception $e){
            DB::rollback();

            return response()->json([
                'status' => false,
                'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'message' => 'Kategori HUB gagal diubah'
            ]);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = KategoriHub::query()->findOrFail($id);
        $data->delete();

        return response()->json([
            'status' => true,
            'code' => Response::HTTP_OK,
            'message' => 'Kategori HUB berhasil dihapus',
        ]);

    }
}
