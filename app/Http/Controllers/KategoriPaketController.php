<?php

namespace App\Http\Controllers;

use App\KategoriPaket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KategoriPaketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = KategoriPaket::all();
        return view('jenis_paket.index', ['datas'=>$datas]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->id;
        $jenis = $request->jenis;
        $point = $request->point;

        try {
            DB::beginTransaction();
            if ($id == null){
                $HUBcreate = new KategoriPaket();
            }else{
                $HUBcreate = KategoriPaket::find($id);
            }
            $HUBcreate->kategori = $jenis;
            $HUBcreate->point = $point;
            $HUBcreate->save();

            DB::commit();
        }catch (\Exception $exception){
            return response()->json(['msg'=>'error simpan data', 'code'=>'0']);
        }

        return response()->json(['msg'=>'berhasil', 'code'=>'1']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = KategoriPaket::findOrFail($id);
        return response()->json(['data'=>$data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            DB::beginTransaction();
            $hub = KategoriPaket::findOrFail($request->id);
            $hub->delete();

            DB::commit();
        }catch (\Exception $exception){
            return response()->json(['msg'=>'error simpan data', 'code'=>'0']);
        }

        return response()->json(['msg'=>'berhasil', 'code'=>'1']);
    }
}
