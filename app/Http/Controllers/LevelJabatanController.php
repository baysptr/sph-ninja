<?php

namespace App\Http\Controllers;

use App\LevelJabatan;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class LevelJabatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = LevelJabatan::all();
        return response()->json([
            'status' => false,
            'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
            'data' => $data
        ]);
    }

    public function create()
    {
        $data = LevelJabatan::all();
        return view('jabatan.index', ['jabatans'=>$data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'nama_level' => 'required',
            'persenan' => 'required',
            'bonus_angka' => 'required',
            'komisi_sewa_motor' => 'required',
        ];

        $ruleMessages = [
            'nama_level.required' => 'Level Jabatan harus diisi',
            'bonus_angka.required' => 'Bonus Angka harus diisi',
            'persenan.required' => 'Persenan harus diisi',
            'komisi_sewa_motor.required' => 'Komisi Sewa Motor harus diisi',
        ];

        $validator = Validator::make($request->all(), $rules, $ruleMessages);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'message' => $validator->errors()->all(),
            ]);
        }

        DB::beginTransaction();

        try {
            if($request->id == null || !isset($request->id)){
                $kh = new LevelJabatan();
            }else{
                $kh = LevelJabatan::find($request->id);
            }
            $kh->nama_level = $request->nama_level;
            $kh->persenan = $request->persenan;
            $kh->bonus_angka = $request->bonus_angka;
            $kh->komisi_sewa_motor = $request->komisi_sewa_motor;
            $kh->save();
            DB::commit();

            return response()->json([
                'status' => true,
                'code' => Response::HTTP_OK,
                'data' => 'Level Jabatan berhasil ditambahkan'
            ]);

        }catch (\Exception $e){
            DB::rollback();

            return response()->json([
                'status' => false,
                'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'message' => 'Level Jabatan gagal ditambahkan'
            ]);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = LevelJabatan::query()->findOrFail($id);
        return response()->json([
            'status' => false,
            'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'nama_level' => 'required',
            'persenan' => 'required',
            'bonus_angka' => 'required',
            'komisi_sewa_motor' => 'required',
        ];

        $ruleMessages = [
            'nama_level.required' => 'Level Jabatan harus diisi',
            'bonus_angka.required' => 'Bonus Angka harus diisi',
            'persenan.required' => 'Persenan harus diisi',
            'komisi_sewa_motor.required' => 'Komisi Sewa Motor harus diisi',
        ];

        $validator = Validator::make($request->all(), $rules, $ruleMessages);

        if ($validator->fails()) {
            return response()->json([
                'status' => false,
                'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'message' => $validator->errors()->all(),
            ]);
        }

        DB::beginTransaction();

        try {
            $kh = LevelJabatan::query()->findOrFail($id);
            $kh->nama_level = $request->nama_level;
            $kh->persenan = $request->persenan;
            $kh->bonus_angka = $request->bonus_angka;
            $kh->komisi_sewa_motor = $request->komisi_sewa_motor;
            $kh->save();
            DB::commit();

            return response()->json([
                'status' => true,
                'code' => Response::HTTP_OK,
                'data' => 'Level Jabatan berhasil diubah'
            ]);

        }catch (\Exception $e){
            DB::rollback();

            return response()->json([
                'status' => false,
                'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'message' => 'Level Jabatan gagal diubah'
            ]);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = LevelJabatan::query()->findOrFail($id);
        $data->delete();

        return response()->json([
            'status' => true,
            'code' => Response::HTTP_OK
        ]);
    }

    public function delete(Request $request)
    {
        $data = LevelJabatan::query()->findOrFail($request->id);
        $data->delete();

        return response()->json([
            'status' => true,
            'code' => Response::HTTP_OK
        ]);
    }
}
