<?php

namespace App\Http\Controllers;

use App\KategoriPaket;
use App\Paket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PaketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Paket::all();
        $jenis = KategoriPaket::all();
        return view('paket.index', [
            'datas'=>$data,
            'jenis'=>$jenis
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->id;
        $jenis = $request->jenis;
        $paket = $request->paket;

        try {
            DB::beginTransaction();
            if ($id == null){
                $HUBcreate = new Paket();
            }else{
                $HUBcreate = Paket::find($id);
            }
            $HUBcreate->kategori_paket_id = $jenis;
            $HUBcreate->nama_paket = $paket;
            $HUBcreate->save();

            DB::commit();
        }catch (\Exception $exception){
            return response()->json(['msg'=>'error simpan data', 'code'=>'0']);
        }

        return response()->json(['msg'=>'berhasil', 'code'=>'1']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Paket::findOrFail($id);
        return response()->json(['data'=>$data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            DB::beginTransaction();
            $hub = Paket::findOrFail($request->id);
            $hub->delete();

            DB::commit();
        }catch (\Exception $exception){
            return response()->json(['msg'=>'error simpan data', 'code'=>'0']);
        }

        return response()->json(['msg'=>'berhasil', 'code'=>'1']);
    }
}
