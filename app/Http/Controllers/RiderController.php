<?php

namespace App\Http\Controllers;

use App\User;
use App\KategoriHub;
use App\LevelJabatan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class RiderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $riders = User::all();
        $stations = KategoriHub::all();
        $levels = LevelJabatan::all();
        $roles = Role::all();
        return view(
            'rider.index',
            [
                'datas' => $riders,
                'stations' => $stations,
                'levels' => $levels,
                'roles' => $roles
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->id;
        $station = $request->station;
        $level = $request->level;
        $nama = $request->nama;
        $rider_id = $request->rider_id;
        $no_wa = $request->no_wa;
        $status = $request->status;
        $role = $request->role;
        $password = Hash::make($request->password);

        try {
            DB::beginTransaction();
            if ($id == null) {
                $user = new User();
                $user->password = $password;
                $user->assignRole($role);
            } else {
                $user = User::find($id);
                $user->syncRoles($role);
            }
            $user->jabatan_id = $level;
            $user->kategori_hub_id = $station;
            $user->nama = $nama;
            $user->no_wa = $no_wa;
            $user->rider_id = $rider_id;
            $user->status = $status;
            $user->save();

            DB::commit();
            return response()->json(['msg' => 'berhasil', 'code' => '1']);
        } catch (\Exception $exception) {
            Log::error($exception);
            DB::rollBack();
            return response()->json(['msg' => 'error simpan data', 'code' => '0']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $rider = User::with('roles')->findOrFail($id);
        return response()->json(['data' => $rider]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            DB::beginTransaction();
            $hub = User::findOrFail($request->id);
            $hub->delete();

            DB::commit();
        } catch (\Exception $exception) {
            return response()->json(['msg' => 'error simpan data', 'code' => '0']);
        }

        return response()->json(['msg' => 'berhasil', 'code' => '1']);
    }

    public function reset(Request $request){
        $password = $request->reset_password;
        $id = $request->id_pass;

        try {
            DB::beginTransaction();

            $rider = User::find($id);
            $rider->password = Hash::make($password);
            $rider->save();

            DB::commit();
            return response()->json(['msg' => 'berhasil', 'code' => '1']);
        } catch (\Exception $exception) {
            Log::error($exception);
            DB::rollBack();
            return response()->json(['msg' => 'error simpan data', 'code' => '0']);
        }
    }
}
