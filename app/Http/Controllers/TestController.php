<?php

namespace App\Http\Controllers;

use App\Traits\SendMessageBroker;
use Illuminate\Http\Request;

class TestController extends Controller
{
    use SendMessageBroker;
    public function wa(){
        $this->notificationWhatsapp('081341332882', 'test wa');
    }
}
