<?php

namespace App\Http\Controllers;

use App\KategoriPaket;
use App\User;
use App\Transaksi;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Traits\SendMessageBroker;

class TransaksiController extends Controller
{
    use SendMessageBroker;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $riders = User::query()->where('kategori_hub_id', Auth::user()->kategori_hub_id)->get();
        $riders = User::query()
            ->where('kategori_hub_id', Auth::user()->kategori_hub_id)
            ->whereHas('roles', function ($q){
                return $q->where('name', 'RIDER');
            })->get();
        $kategoris = KategoriPaket::all();
        $transaksis = Transaksi::all();
        return view('transaksi.index', [
            'riders'=>$riders,
            'kategoris'=>$kategoris,
            'transaksis'=>$transaksis
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $id = $request->id;
            $tanggal = $request->tanggal;
            $rider_id = $request->rider;

            $kategori_id = [];
            $kategori_nama = [];
            $kategori_pengali = [];
            $kuantiti = [];
            $point = 0;
            $kategoris = KategoriPaket::all();
            foreach ($kategoris as $kategori){
                $point_temp = $kategori->point;
                $jumlah = $request->post('kuantiti_'.$kategori->id);

                $point = $point + ($point_temp * $jumlah);

                array_push($kategori_id, $request->post('id_kategori_'.$kategori->id));
                array_push($kategori_nama, $kategori->kategori);
                array_push($kategori_pengali, $point_temp);
                array_push($kuantiti, $jumlah);
            }

            $rider = User::find($rider_id);
            $umk = $rider->hub->umk;

            $level_rider = $rider->jabatan->persenan;
            $komisi_sewa_motor = $rider->jabatan->komisi_sewa_motor;

            $user_post_id = Auth::user()->id;
            $user_post_nama = Auth::user()->nama;
            $user_post_no_wa = Auth::user()->no_wa;

            if($point>150){
                $gaji = ((150 / 100) * $umk) + ($point * 500);
            }elseif ($point>=110&&$point<150){
                $gaji = ((130 / 100) * $umk) + ($point * 500);
            }elseif ($point>=80&&$point<110){
                $gaji = ((100 / 100) * $umk) + ($point * 500);
            }elseif ($point>=60&&$point<80){
                $gaji = ((65 / 100) * $umk) + ($point * 500);
            }elseif ($point>=40&&$point<60){
                $gaji = ((45 / 100) * $umk) + ($point * 500);
            }elseif ($point>=20&&$point<40){
                $gaji = ((30 / 100) * $umk) + ($point * 500);
            }else{
                // $ocd < 20
                $gaji = ($point * 500);
            }
            $gaji = round($gaji, 0, PHP_ROUND_HALF_ODD);

            $gaji_kategori = round(($gaji * $level_rider),0,PHP_ROUND_HALF_ODD);
            $gaji_total = round(($gaji_kategori + $komisi_sewa_motor),0,PHP_ROUND_HALF_ODD);

            DB::beginTransaction();
            if ($id == null){
                $transaksi = new Transaksi();
            }else{
                $transaksi = Transaksi::find($id);
            }
            $transaksi->tanggal = $tanggal;
            $transaksi->rider_id = $rider_id;
            $transaksi->kategori_paket_id = $kategori_id;
            $transaksi->nama_kategori_paket = $kategori_nama;
            $transaksi->pengali_kategori_paket = $kategori_pengali;
            $transaksi->kuantiti = $kuantiti;
            $transaksi->umk_hub = $umk;
            $transaksi->ocd = $point;
            $transaksi->gaji = $gaji;
            $transaksi->gaji_kategori = $gaji_kategori;
            $transaksi->gaji_total = $gaji_total;
            $transaksi->komisi_sewa = $komisi_sewa_motor;
            $transaksi->posting_id = $user_post_id;
            $transaksi->posting_nama = $user_post_nama;
            $transaksi->posting_no_wa = $user_post_no_wa;
            $transaksi->save();
            DB::commit();

//            try {
//                $pesan = "-SPH Ninja Xpress-
//Selamat, kepada *" . $rider->nama . "*
//pada " . date('d F Y', strtotime($tanggal)) . " anda membawa paket " . $kuantiti[0] . " " . $kategori_nama[0] . ' dan ' . $kuantiti[1] . " " . $kategori_nama[1] . " dengan total point yang anda peroleh *" . $point . "*";
//                $this->notificationWhatsapp($rider->no_wa, $pesan);
//            }catch (\Exception $e){
//                return response()->json([
//                    'status' => false,
//                    'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
//                    'message' => "Gagal Mengirimkan notifikasi, namun berhasil input data transaksi",
//                    'system'=>$e
//                ]);
//            }

            return response()->json([
                'status' => true,
                'code' => 200,
                'transaksi' => $transaksi,
            ]);

        }catch (\Exception $e){
            return response()->json([
                'status' => false,
                'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'message' => "Maaf data yang anda masukan kurang tepat, antara kuantiti dengan paket anda jumlahnya tidak sama / tidak sinkron",
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Transaksi::findOrFail($id);
        return response()->json(['data'=>$data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            DB::beginTransaction();
            $hub = Transaksi::findOrFail($request->id);
            $hub->delete();

            DB::commit();
        }catch (\Exception $exception){
            return response()->json(['msg'=>'error simpan data', 'code'=>'0']);
        }

        return response()->json(['msg'=>'berhasil', 'code'=>'1']);
    }

    public function data(Request $request){
        $transaksis = Transaksi::query()->whereDate('tanggal', $request->tanggal)->get();
        if(count($transaksis) > 0){
            $response = '';
            foreach($transaksis as $transaksi){
                    $response = $response . '<tr>
                        <td>'.$transaksi->rider->nama.'</td>
                        <td>'.$transaksi->rider->jabatan->nama_level.'</td>
                        <td>'.$transaksi->kuantiti[0].'</td>
                        <td>'.$transaksi->kuantiti[1].'</td>
                        <td>'.$transaksi->ocd.'</td>
                        <td>Rp. '.number_format($transaksi->gaji,0,",",".").'</td>
                        <td>Rp. '.number_format($transaksi->gaji_kategori,0,",",".").'</td>
                        <td>Rp. '.number_format($transaksi->komisi_sewa,0,",",".").'</td>
                        <td>Rp. '.number_format($transaksi->gaji_total,0,",",".").'</td>
                        <td><a href="#" onclick="editHub('.$transaksi->id.')" class="btn btn-block btn-info"><i class="fa fa-edit"></i></a></td>
                        <td><a href="#" onclick="hapusHub('.$transaksi->id.')" class="btn btn-block btn-warning"><i class="fa fa-trash"></i></a></td>
                    </tr>';
            }
            return response()->json(['data'=>$response]);
        }else{
            $t = '<tr><td colspan="11" class="text-center"><strong>Tidak ada transaksi di tanggal tersebut</strong></td></tr>';
            return response()->json(['data'=>$t]);
        }
    }
}
