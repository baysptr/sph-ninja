<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriHub extends Model
{
    public function users(){
        return $this->hasMany('User');
    }
}
