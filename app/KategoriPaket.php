<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriPaket extends Model
{
    public function paket(){
        return $this->hasMany('App\Paket');
    }
}
