<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paket extends Model
{
    public function kategori(){
        return $this->belongsTo('App\KategoriPaket', 'kategori_paket_id');
    }
}
