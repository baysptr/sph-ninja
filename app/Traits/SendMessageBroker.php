<?php

namespace App\Traits;

use Bschmitt\Amqp\Amqp;

trait SendMessageBroker
{
    public function publishMessage($routing, $data, $exchange)
    {
        $pub = new Amqp();

        $pub->publish($routing, json_encode($data), [
            'exchange' => $exchange,
            'exchange_type' => 'direct',
            'exchange_durable' => true,
            'exchange_auto_delete' => true,
        ]);

        return true;
    }

    public function notificationWhatsapp($number, $messages)
    {
        $data = [
            'notif_wa' => true,
            'number' => $number,
            'pesan' => $messages,
        ];

        return $this->publishMessage('wa', $data, 'notifikasi');
    }

}
