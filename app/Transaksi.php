<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    protected $casts = [
        'kategori_paket_id'=>'array',
        'nama_kategori_paket'=>'array',
        'pengali_kategori_paket'=>'array',
        'kuantiti'=>'array',
    ];

    public function rider(){
        return $this->belongsTo('App\User', 'rider_id');
    }
}
