<?php

namespace App;

use Spatie\Permission\Traits\HasRoles;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasRoles;

    public function jabatan()
    {
        return $this->belongsTo('App\LevelJabatan', 'jabatan_id');
    }

    public function hub()
    {
        return $this->belongsTo('App\KategoriHub', 'kategori_hub_id');
    }

    public function transaksi(){
        return$this->hasMany('App\Transaksi');
    }
}
