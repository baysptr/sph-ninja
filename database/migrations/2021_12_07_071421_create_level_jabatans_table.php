<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLevelJabatansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('level_jabatans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_level');
            $table->double('persenan');
            $table->double('bonus_angka');
            $table->double('komisi_sewa_motor');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('level_jabatans');
    }
}
