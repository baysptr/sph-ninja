<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKategoriHubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kategori_hubs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('gudang');
            $table->string('wilayah');
            $table->string('kabkot');
            $table->string('kecamatan');
            $table->string('umk');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kategori_hubs');
    }
}
