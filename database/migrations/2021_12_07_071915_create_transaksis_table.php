<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransaksisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksis', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('tanggal');
            $table->bigInteger('rider_id');
            $table->json('kategori_paket_id');
            $table->json('nama_kategori_paket');
            $table->json('pengali_kategori_paket');
            $table->json('kuantiti');
            $table->double('umk_hub');
            $table->double('ocd');
            $table->double('gaji');
            $table->double('gaji_kategori');
            $table->double('komisi_sewa');
            $table->double('gaji_total');
            $table->bigInteger('posting_id');
            $table->string('posting_nama');
            $table->string('posting_no_wa');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksis');
    }
}
