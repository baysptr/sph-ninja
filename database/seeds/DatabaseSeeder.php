<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(KategoriHubSeeder::class);
        $this->call(KategoriPaketSeeder::class);
        $this->call(LevelJabatanSeeder::class);
        $this->call(PaketSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);
    }
}
