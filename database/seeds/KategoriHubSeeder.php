<?php

use Illuminate\Database\Seeder;

class KategoriHubSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $batch = [
            [
                'gudang'=>'SUB',
                'wilayah'=>'SDA',
                'kabkot'=>'Sidoarjo',
                'kecamatan'=>'Waru',
                'umk'=>'165270'
            ]
        ];

        \Illuminate\Support\Facades\DB::table('kategori_hubs')->insert($batch);
    }
}
