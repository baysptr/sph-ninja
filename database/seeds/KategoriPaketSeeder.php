<?php

use Illuminate\Database\Seeder;

class KategoriPaketSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $batch = [
            [
                'kategori'=>'SMALL',
                'point'=>1
            ],
            [
                'kategori'=>'BULKY',
                'point'=>2.5
            ]
        ];

        \Illuminate\Support\Facades\DB::table('kategori_pakets')->insert($batch);
    }
}
