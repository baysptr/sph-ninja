<?php

use Illuminate\Database\Seeder;

class LevelJabatanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $batch = [
            [
                "nama_level"=>"Perunggu",
                "persenan"=>1,
                "bonus_angka"=>0,
                "komisi_sewa_motor"=>0
            ],[
                "nama_level"=>"Perak",
                "persenan"=>1,
                "bonus_angka"=>0,
                "komisi_sewa_motor"=>0
            ],[
                "nama_level"=>"Emas",
                "persenan"=>1.05,
                "bonus_angka"=>100000,
                "komisi_sewa_motor"=>13000
            ],[
                "nama_level"=>"Platinum",
                "persenan"=>1.15,
                "bonus_angka"=>300000,
                "komisi_sewa_motor"=>13000
            ],[
                "nama_level"=>"Berlian",
                "persenan"=>1.2,
                "bonus_angka"=>500000,
                "komisi_sewa_motor"=>13000
            ]
        ];
        \Illuminate\Support\Facades\DB::table('level_jabatans')->insert($batch);
    }
}
