<?php

use Illuminate\Database\Seeder;

class PaketSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $batch = [
            [
                'kategori_paket_id'=>1,
                'nama_paket'=>'S'
            ],
            [
                'kategori_paket_id'=>1,
                'nama_paket'=>'M'
            ],
            [
                'kategori_paket_id'=>2,
                'nama_paket'=>'L'
            ],
            [
                'kategori_paket_id'=>2,
                'nama_paket'=>'XL'
            ],
        ];

        \Illuminate\Support\Facades\DB::table('pakets')->insert($batch);
    }
}
