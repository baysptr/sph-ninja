<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $batch = [
            [
                'jabatan_id' => 1,
                'kategori_hub_id' => 1,
                'nama' => 'Admin',
                'no_wa' => '089677271257',
                'rider_id' => 'xxxxxxxxx',
                'password' => Hash::make('password'),
                'status' => 'aktif',
            ],
            [
                'jabatan_id' => 3,
                'kategori_hub_id' => 1,
                'nama' => 'Rider 1',
                'no_wa' => '089677271256',
                'rider_id' => 'xxxxxxxxx',
                'password' => Hash::make('password'),
                'status' => 'aktif',
            ],
        ];

        DB::table('users')->insert($batch);

        $roles = [
            [
                'role_id' => 1,
                'model_type' => 'App\User',
                'model_id' => 1,
            ],
            [
                'role_id' => 3,
                'model_type' => 'App\User',
                'model_id' => 2,
            ],
        ];

        DB::table('model_has_roles')->insert($roles);
    }
}
