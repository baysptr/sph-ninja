@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Form Login') }}</div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <lottie-player src="https://assets4.lottiefiles.com/packages/lf20_nioprtrk.json"
                            background="transparent" speed="2" style="width: 100%; height: auto; margin: 0 auto;" loop
                            autoplay></lottie-player>
                        </div>
                        <div class="col-sm-6">
                            <form method="POST" action="{{ route('login') }}">
                                @csrf
        
                                <div class="form-group row">
                                    <label for="no_wa" class="col-md-4 col-form-label text-md-right">{{ __('No Whatsapp')
                                        }}</label>
        
                                    <div class="col-md-6">
                                        <input id="no_wa" type="number"
                                            class="form-control @error('no_wa') is-invalid @enderror" name="no_wa"
                                            value="{{ old('no_wa') }}" autocomplete="no_wa" autofocus>
        
                                        @error('no_wa')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
        
                                <div class="form-group row">
                                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password')
                                        }}</label>
        
                                    <div class="col-md-6">
                                        <input id="password" type="password"
                                            class="form-control @error('password') is-invalid @enderror" name="password"
                                            autocomplete="current-password">
        
                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
        
                                <div class="form-group row mb-0">
                                    <div class="col-md-8 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Login') }}
                                        </button>
        
                                        @if (Route::has('password.request'))
                                        <a class="btn btn-link" href="{{ route('password.request') }}">
                                            {{ __('Forgot Your Password?') }}
                                        </a>
                                        @endif
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection