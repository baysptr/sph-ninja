@extends('layouts.app')

@section('content')
    <div class="bg-light p-5 rounded">
        <div class="form-group">
            <label>Pilih Bulan</label>
            <input type="month" class="form-control" onchange="drawSpline(this)">
        </div>
        <div class="row">
            <div class="col-md-12"><div id="spline"></div><br/></div>
            <div class="col-md-12"><div id="piefs">test</div></div>
        </div>
    </div>
@endsection

@section('footer-script')
    <script>
        function getBulan(number){
            const month = ["Januari","Febuari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","December"];
            return month[number];
        }

        function getColorPattern(i) {
            var colors = Highcharts.getOptions().colors,
                patternColors = [colors[2], colors[0], colors[3], colors[1], colors[4]],
                patterns = [
                    'M 0 0 L 5 5 M 4.5 -0.5 L 5.5 0.5 M -0.5 4.5 L 0.5 5.5',
                    'M 0 5 L 5 0 M -0.5 0.5 L 0.5 -0.5 M 4.5 5.5 L 5.5 4.5',
                    'M 1.5 0 L 1.5 5 M 4 0 L 4 5',
                    'M 0 1.5 L 5 1.5 M 0 4 L 5 4',
                    'M 0 1.5 L 2.5 1.5 L 2.5 0 M 2.5 5 L 2.5 3.5 L 5 3.5'
                ];
            return {
                pattern: {
                    path: patterns[i],
                    color: patternColors[i],
                    width: 5,
                    height: 5
                }
            };
        }

        function mxArray(arr) {
            return Math.max.apply(Math, arr.map(function(o) { return o.y; }));
        }

        function minArray(arr) {
            return Math.min.apply(Math, arr.map(function(o) { return o.y; }));
        }

        function drawSpline(month){
            var d = new Date();
            var param = '';
            var judul = '';
            var judulPie = '';
            if (!month.value){
                param = d.getFullYear() + '-' + (d.getMonth()+1);
                judul = "Grafik Perolehan Point Rider Bulan "+getBulan(d.getMonth());
                judulPie = "Persentase Perolehan Point Rider Bulan "+getBulan(d.getMonth());
            }else{
                param = month.value
                var txt = param.split('-')
                judul = "Grafik Perolehan Point Rider Bulan "+getBulan((parseInt(txt[1]) - 1));
                judulPie = "Persentase Perolehan Point Rider Bulan "+getBulan((parseInt(txt[1]) - 1));
            }

            $.getJSON("/dashboard/rider/"+param, function (data) {
                var series = [];
                for (var i=0;i<data.length;i++){
                    // console.log(data[i]['data'][0][0]);
                    var row = [];
                    row['name'] = data[i]['name'];
                    var drow = [];
                    for (var j=0;j<data[i]['data'].length; j++){
                        var d = new Date(data[i]['data'][j][0]).getTime();
                        var v = data[i]['data'][j][1];
                        drow.push([d, v]);
                    }
                    row['data'] = drow;
                    // row['dashStyle'] = styleDash();
                    series.push(row);
                }
                // ====================================================
                // Draw Line CHart
                // ====================================================
                Highcharts.chart('spline', {
                    title: {
                        text: judul
                    },
                    yAxis: {
                        title: {
                            text: 'Pendapatan'
                        },
                        allowDecimals: false,
                        // labels: {
                        //     formatter: function (){
                        //         return 'Rp. ' + (this.value/1000).toFixed(3);
                        //     }
                        // }
                    },
                    tooltip: {
                        formatter: function () {
                            return '<b>' + this.series.name + '</b><br/>' +
                                Highcharts.dateFormat('%e %b', this.x)  + ': <strong>' + this.y + '</strong> Point';
                        },
                        borderRadius: 10,
                        borderWidth: 3,
                        crosshairs: [true, true]
                    },
                    plotOptions: {
                        series: {
                            label: {
                                connectorAllowed: false
                            },
                            lineWidth: 4,
                        }
                    },
                    xAxis: {
                        type: 'datetime',
                        labels: {
                            formatter: function() {
                                var monthStr = Highcharts.dateFormat('%e %b', this.value);
                                // var firstLetter = monthStr.substring(0, 1);
                                return monthStr;
                            }
                        },
                    },
                    series: series
                });
            });

            $.getJSON("/dashboard/rider/pie/"+param, function (data) {
                var series = [];
                var seriesBar = [];
                var names = [];
                var mxVal = mxArray(data);
                var minVal = mxArray(data);
                // console.log(mxArray(data));
                // console.log(minArray(data));
                for(var i=0;i<data.length;i++){
                    if(mxVal === data[i].y){
                        var row = {name: data[i].name, color: getColorPattern(i), y: data[i].y, sliced: true, selected: true};
                    }else if(minVal === data[i].y){
                        var row = {name: data[i].name, color: getColorPattern(i), y: data[i].y, sliced: true, selected: true};
                    }else {
                        var row = {name: data[i].name, color: getColorPattern(i), y: data[i].y};
                    }
                    var rowBar = {name: data[i].name, color: getColorPattern(i), y: data[i].y};
                    // var row = {name: data[i].name, color: getColorPattern(i), y: data[i].y, sliced: true, selected: true};
                    names.push(data[i].name);
                    series.push(row);
                    seriesBar.push(rowBar);
                }
                // console.log(series);
                // ====================================================
                // Draw Pie CHart
                // ====================================================
                Highcharts.chart('piefs', {
                    chart: {
                        type: 'pie'
                    },
                    title: {
                        text: judulPie
                    },
                    tooltip: {
                        formatter: function () {
                            return '<b>' + this.point.name + '</b><br/>' +
                                '<strong>' + this.point.y + '</strong> Point';
                        },
                        borderRadius: 10,
                        borderWidth: 3,
                        crosshairs: [true]
                    },
                    plotOptions: {
                        series: {
                            dataLabels: {
                                enabled: true,
                                connectorColor: '#777',
                                format: '<b>{point.name}</b>: {point.percentage:.2f} %'
                            },
                            cursor: 'pointer',
                            borderWidth: 3
                        }
                    },
                    series: [{
                        name: 'Intensitas',
                        data: series
                    }]
                });
            });

        }

        drawSpline(0);
    </script>
@endsection
