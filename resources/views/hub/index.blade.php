@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <h2>Daftar HUB</h2>
        </div>
        <table class="table table-striped">
            <thead class="table-dark">
                <tr>
                    <td>Gudang</td>
                    <td>Mini Station</td>
                    <td>Kabupaten / Kota</td>
                    <td>Kecamatan</td>
                    <td>UMK</td>
                    <td colspan="2">
                        <button onclick="showModal()" class="btn btn-block btn-primary">Tambah Data!</button>
                    </td>
                </tr>
            </thead>
            <tbody>
                @foreach($hubs as $hub)
                    <tr>
                        <td>{{$hub->gudang}}</td>
                        <td>{{$hub->wilayah}}</td>
                        <td>{{$hub->kabkot}}</td>
                        <td>{{$hub->kecamatan}}</td>
                        <td>{{$hub->umk}}</td>
                        <td><a href="#" onclick="editHub('{{$hub->id}}')" class="btn btn-block btn-info">Ubah</a></td>
                        <td><a href="#" onclick="hapusHub('{{$hub->id}}')" class="btn btn-block btn-warning">Hapus</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="hubModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" onclick="closeModal()" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form name="hubFrom" id="hubForm">
                        @csrf
                        <input type="hidden" name="id" id="id">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Gudang</label>
                                    <input type="text" class="form-control" name="gudang" id="gudang">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Wilayah</label>
                                    <input type="text" class="form-control" name="wilayah" id="wilayah">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>UMK</label>
                                    <input type="text" class="form-control" name="umk" id="umk">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Kabupaten / Kota</label>
                                    <textarea name="kabkot" id="kabkot" class="form-control" rows="5"></textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Kecamatan</label>
                                    <textarea name="kecamatan" id="kecamatan" class="form-control" rows="5"></textarea>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" onclick="closeModal()">Close</button>
                    <button type="button" class="btn btn-primary" onclick="simpanHub()">Simpan!</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer-script')
    <script>
        function showModal(){
            $(".modal-title").html('Form Tambah Wilayah Station');
            $("#hubModal").modal('show');
        }

        function closeModal(){
            $("#id").val("");
            $("#gudang").val("");
            $("#wilayah").val("");
            $("#kabkot").val("");
            $("#kecamatan").val("");
            $("#umk").val("");
            $("#hubModal").modal('hide');
        }

        function editHub(id){
            $.ajax({
                url: "/hub/show/"+id,
                type: "GET",
                dataType: 'json',
                success: function (data){
                    $("#id").val(data.data.id);
                    $("#gudang").val(data.data.gudang);
                    $("#wilayah").val(data.data.wilayah);
                    $("#kabkot").val(data.data.kabkot);
                    $("#kecamatan").val(data.data.kecamatan);
                    $("#umk").val(data.data.umk);
                    $(".modal-title").html('Form Ubah Wilayah Station');
                    $("#hubModal").modal('show');
                }
            });
        }

        function hapusHub(id){
            if(confirm('Apakah anda yakin hapus data ini ?') === true){
                $.ajax({
                    url: "{{route('hub.destroy')}}",
                    type: "POST",
                    dataType: 'json',
                    data: {
                        _token: '{{csrf_token()}}',
                        id: id
                    },
                    success: function (data){
                        if(data.code==='0'){
                            alert(data.msg);
                        }else{
                            location.reload();
                        }
                    }
                });
            }
        }

        function simpanHub(){
            $.ajax({
               url: "{{route('hub.store')}}",
               type: "POST",
               dataType: 'json',
               data: $("#hubForm").serialize(),
               success: function (data){
                   if(data.code==='0'){
                       closeModal();
                       alert(data.msg);
                   }else{
                       location.reload();
                   }
               }
            });
        }
    </script>
@endsection
