@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <h2>Daftar Jabatan Rider</h2>
        </div>
        <table class="table table-striped">
            <thead class="table-dark">
                <tr>
                    <td>Jabatan</td>
                    <td>Pengali Point</td>
                    <td>Bonus</td>
                    <td>Sewa Motor</td>
                    <td colspan="2">
                        <button onclick="showModal()" class="btn btn-block btn-primary">Tambah Data!</button>
                    </td>
                </tr>
            </thead>
            <tbody>
                @foreach($jabatans as $data)
                    <tr>
                        <td>{{$data->nama_level}}</td>
                        <td>{{$data->persenan}} x Point</td>
                        <td>{{$data->bonus_angka}}</td>
                        <td>{{$data->komisi_sewa_motor}}</td>
                        <td><a href="#" onclick="editHub('{{$data->id}}')" class="btn btn-block btn-info">Ubah</a></td>
                        <td><a href="#" onclick="hapusHub('{{$data->id}}')" class="btn btn-block btn-warning">Hapus</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="hubModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" onclick="closeModal()" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form name="hubFrom" id="hubForm">
                        @csrf
                        <input type="hidden" name="id" id="id">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nama Jabatan</label>
                                    <input type="text" class="form-control" name="nama_level" id="jabatan">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Persenan (x Point)</label>
                                    <input type="text" class="form-control" name="bonus_angka" id="persenan">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Bonus</label>
                                    <input type="text" class="form-control" name="persenan" id="bonus">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Komisi Sewa Motor</label>
                                    <input type="text" class="form-control" name="komisi_sewa_motor" id="sewa">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" onclick="closeModal()">Close</button>
                    <button type="button" class="btn btn-primary" onclick="simpanHub()">Simpan!</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer-script')
    <script>
        function showModal(){
            $(".modal-title").html('Form Tambah Jabatan Rider');
            $("#hubModal").modal('show');
        }

        function closeModal(){
            $("#id").val("");
            $("#id").val("");
            $("#jabatan").val("");
            $("#persenan").val("");
            $("#bonus").val("");
            $("#sewa").val("");
            $("#hubModal").modal('hide');
        }

        function editHub(id){
            $.ajax({
                url: "/level-jabatan/show/"+id,
                type: "GET",
                dataType: 'json',
                success: function (data){
                    $("#id").val(data.data.id);
                    $("#jabatan").val(data.data.nama_level);
                    $("#persenan").val(data.data.persenan);
                    $("#bonus").val(data.data.bonus_angka);
                    $("#sewa").val(data.data.komisi_sewa_motor);
                    $(".modal-title").html('Form Ubah Jabatan Rider');
                    $("#hubModal").modal('show');
                }
            });
        }

        function hapusHub(id){
            if(confirm('Apakah anda yakin hapus data ini ?') === true){
                $.ajax({
                    url: "{{route('level-jabatan.destroy')}}",
                    type: "POST",
                    dataType: 'json',
                    data: {
                        _token: '{{csrf_token()}}',
                        id: id
                    },
                    success: function (data){
                        if(data.code==='0'){
                            alert(data.msg);
                        }else{
                            location.reload();
                        }
                    }
                });
            }
        }

        function simpanHub(){
            $.ajax({
               url: "{{route('level-jabatan.store')}}",
               type: "POST",
               dataType: 'json',
               data: $("#hubForm").serialize(),
               success: function (data){
                   if(data.code==='0'){
                       closeModal();
                       alert(data.msg);
                   }else{
                       location.reload();
                   }
               }
            });
        }
    </script>
@endsection
