@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <h2>Daftar Jenis Paket</h2>
        </div>
        <table class="table table-striped">
            <thead class="table-dark">
                <tr>
                    <td>Jenis Paket</td>
                    <td>Point Paket</td>
                    <td colspan="2">
                        <button onclick="showModal()" class="btn btn-block btn-primary">Tambah Data!</button>
                    </td>
                </tr>
            </thead>
            <tbody>
                @foreach($datas as $data)
                    <tr>
                        <td>{{$data->kategori}}</td>
                        <td>{{$data->point}}</td>
                        <td><a href="#" onclick="editHub('{{$data->id}}')" class="btn btn-block btn-info">Ubah</a></td>
                        <td><a href="#" onclick="hapusHub('{{$data->id}}')" class="btn btn-block btn-warning">Hapus</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="hubModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" onclick="closeModal()" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form name="hubFrom" id="hubForm">
                        @csrf
                        <input type="hidden" name="id" id="id">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Jenis Paket</label>
                                    <input type="text" class="form-control" name="jenis" id="jenis">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Point</label>
                                    <input type="text" class="form-control" name="point" id="point">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" onclick="closeModal()">Close</button>
                    <button type="button" class="btn btn-primary" onclick="simpanHub()">Simpan!</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer-script')
    <script>
        function showModal(){
            $(".modal-title").html('Form Tambah Jenis Paket');
            $("#hubModal").modal('show');
        }

        function closeModal(){
            $("#id").val("");
            $("#point").val("");
            $("#jenis").val("");
            $("#hubModal").modal('hide');
        }

        function editHub(id){
            $.ajax({
                url: "/jenis-paket/show/"+id,
                type: "GET",
                dataType: 'json',
                success: function (data){
                    $("#id").val(data.data.id);
                    $("#point").val(data.data.point);
                    $("#jenis").val(data.data.kategori);
                    $(".modal-title").html('Form Ubah jenis Paket');
                    $("#hubModal").modal('show');
                }
            });
        }

        function hapusHub(id){
            if(confirm('Apakah anda yakin hapus data ini ?') === true){
                $.ajax({
                    url: "{{route('jenis-paket.destroy')}}",
                    type: "POST",
                    dataType: 'json',
                    data: {
                        _token: '{{csrf_token()}}',
                        id: id
                    },
                    success: function (data){
                        if(data.code==='0'){
                            alert(data.msg);
                        }else{
                            location.reload();
                        }
                    }
                });
            }
        }

        function simpanHub(){
            $.ajax({
               url: "{{route('jenis-paket.store')}}",
               type: "POST",
               dataType: 'json',
               data: $("#hubForm").serialize(),
               success: function (data){
                   if(data.code==='0'){
                       closeModal();
                       alert(data.msg);
                   }else{
                       location.reload();
                   }
               }
            });
        }
    </script>
@endsection
