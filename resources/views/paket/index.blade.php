@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <h2>Daftar Paket</h2>
        </div>
        <table class="table table-striped">
            <thead class="table-dark">
                <tr>
                    <td>Jenis Paket</td>
                    <td>Point Paket</td>
                    <td>Ukuran</td>
                    <td colspan="2">
                        <button onclick="showModal()" class="btn btn-block btn-primary">Tambah Data!</button>
                    </td>
                </tr>
            </thead>
            <tbody>
                @foreach($datas as $data)
                    <tr>
                        <td>{{$data->kategori->kategori}}</td>
                        <td>{{$data->kategori->point}}</td>
                        <td>{{$data->nama_paket}}</td>
                        <td><a href="#" onclick="editHub('{{$data->id}}')" class="btn btn-block btn-info">Ubah</a></td>
                        <td><a href="#" onclick="hapusHub('{{$data->id}}')" class="btn btn-block btn-warning">Hapus</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="hubModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" onclick="closeModal()" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form name="hubFrom" id="hubForm">
                        @csrf
                        <input type="hidden" name="id" id="id">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Jenis Paket</label>
                                    <select name="jenis" id="jenis" class="form-control">
                                        <option selected disabled>-- Pilih Jenis Paket --</option>
                                        @foreach($jenis as $js)
                                            <option value="{{$js->id}}">{{$js->kategori}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nama Paket</label>
                                    <input type="text" class="form-control" name="paket" id="paket">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" onclick="closeModal()">Close</button>
                    <button type="button" class="btn btn-primary" onclick="simpanHub()">Simpan!</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer-script')
    <script>
        function showModal(){
            $(".modal-title").html('Form Tambah Paket');
            $("#hubModal").modal('show');
        }

        function closeModal(){
            $("#id").val("");
            $("#jenis").val("");
            $("#paket").val("");
            $("#hubModal").modal('hide');
        }

        function editHub(id){
            $.ajax({
                url: "/paket/show/"+id,
                type: "GET",
                dataType: 'json',
                success: function (data){
                    $("#id").val(data.data.id);
                    $("#jenis").val(data.data.kategori_paket_id);
                    $("#paket").val(data.data.nama_paket);
                    $(".modal-title").html('Form Ubah Paket');
                    $("#hubModal").modal('show');
                }
            });
        }

        function hapusHub(id){
            if(confirm('Apakah anda yakin hapus data ini ?') === true){
                $.ajax({
                    url: "{{route('paket.destroy')}}",
                    type: "POST",
                    dataType: 'json',
                    data: {
                        _token: '{{csrf_token()}}',
                        id: id
                    },
                    success: function (data){
                        if(data.code==='0'){
                            alert(data.msg);
                        }else{
                            location.reload();
                        }
                    }
                });
            }
        }

        function simpanHub(){
            $.ajax({
               url: "{{route('paket.store')}}",
               type: "POST",
               dataType: 'json',
               data: $("#hubForm").serialize(),
               success: function (data){
                   if(data.code==='0'){
                       closeModal();
                       alert(data.msg);
                   }else{
                       location.reload();
                   }
               }
            });
        }
    </script>
@endsection
