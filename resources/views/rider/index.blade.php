@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <h2>Daftar Rider / Pengguna</h2>
        </div>
        <table class="table table-striped">
            <thead class="table-dark">
                <tr>
                    <td>HUB</td>
                    <td>NAMA / RIDER ID</td>
                    <td>JABATAN RIDER</td>
                    <td>NO. WHATSAPP</td>
                    <td colspan="3">
                        <button onclick="showModal()" class="btn btn-block btn-primary">Tambah Data!</button>
                    </td>
                </tr>
            </thead>
            <tbody>
                @foreach($datas as $data)
                    <tr>
                        <td>{{$data->hub->gudang}} - {{$data->hub->wilayah}}</td>
                        <td>
                            {{$data->nama}}
                            <br/>
                            <small>
                                ID: {{$data->rider_id}}
                                @if($data->status == 'aktif')
                                    <span class="alert-success"><strong> active </strong></span>
                                @else
                                    <span class="alert-dark"><strong> inactive </strong></span>
                                @endif
                            </small>
                        </td>
                        <td>{{$data->jabatan->nama_level}}<br/><small>Role: {{$data->roles[0]->name}}</small></td>
                        <td>{{$data->no_wa}}</td>
                        <td><a href="#" onclick="editHub('{{$data->id}}')" class="btn btn-block btn-info">Ubah</a></td>
                        <td><a href="#" onclick="resetPass('{{$data->id}}')" class="btn btn-block btn-dark">Reset Password</a></td>
                        <td><a href="#" onclick="hapusHub('{{$data->id}}')" class="btn btn-block btn-warning">Hapus</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="hubModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" onclick="closeModal()" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form name="hubFrom" id="hubForm">
                        @csrf
                        <input type="hidden" name="id" id="id">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Station</label>
                                    <select name="station" id="station" class="form-control">
                                        <option selected disabled>-- Pilih Station --</option>
                                        @foreach($stations as $station)
                                            <option value="{{$station->id}}">{{$station->gudang}} - {{$station->wilayah}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Level Rider</label>
                                    <select name="level" id="level" class="form-control">
                                        <option selected disabled>-- Pilih Level Rider --</option>
                                        @foreach($levels as $level)
                                            <option value="{{$level->id}}">{{$level->nama_level}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nama Rider / Pengguna</label>
                                    <input type="text" class="form-control" name="nama" id="nama">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>ID Rider</label>
                                    <input type="text" class="form-control" name="rider_id" id="rider_id">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>No. Whatsapp</label>
                                    <input type="text" class="form-control" name="no_wa" id="no_wa">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="text" class="form-control" name="password" id="password">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Role</label>
                                    <select name="role" id="role" class="form-control">
                                        <option selected disabled>-- Pilih Role Pengguna / Rider --</option>
                                        @role("ADMIN")
                                        <option value="ADMIN">ADMIN</option>
                                        <option value="STAFF">STAFF</option>
                                        <option value="RIDER">RIDER</option>
                                        @endrole

                                        @role("STAFF")
                                        <option value="RIDER">RIDER</option>
                                        @endrole
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Status</label>
                                    <select name="status" id="status" class="form-control">
                                        <option value="aktif">AKTIF</option>
                                        <option value="inaktif">INAKTIF</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" onclick="closeModal()">Close</button>
                    <button type="button" class="btn btn-primary" onclick="simpanHub()">Simpan!</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Reset Password -->
    <div class="modal fade" id="passModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Form Reset Password</h5>
                    <button type="button" class="close" onclick="closeModal()" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form name="hubFrom" id="hubForm">
                        @csrf
                        <input type="hidden" name="id_pass" id="id_pass">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Masukan Password Baru</label>
                                    <input type="text" class="form-control" name="reset_password" id="reset_password">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" onclick="closePass()">Close</button>
                    <button type="button" class="btn btn-primary" onclick="simpanPass()">Simpan!</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer-script')
    <script>
        // $(document).ready(function() {
        //     $('.select2').select2();
        // });

        function showModal(){
            $(".modal-title").html('Form Tambah Rider / Pengguna');
            $("#hubModal").modal('show');
        }

        function closeModal(){
            $("#id").val("");
            $("#station").val("");
            $("#level").val("");
            $("#nama").val("");
            $("#rider_id").val("");
            $("#no_wa").val("");
            $("#password").val("");
            $("#role").val("");
            document.getElementById("password").readOnly = false;
            $("#status").val("");
            $("#hubModal").modal('hide');
        }

        function closePass(){
            $("#id_pass").val("");
            $("#reset_password").val("");
            $("#passModal").modal('hide');
        }

        function resetPass(id){
            $("#id_pass").val(id);
            $("#passModal").modal('show');
        }

        function editHub(id){
            $.ajax({
                url: "/rider/show/"+id,
                type: "GET",
                dataType: 'json',
                success: function (data){
                    console.log(data.data.role);
                    $("#id").val(data.data.id);
                    $("#station").val(data.data.kategori_hub_id);
                    $("#level").val(data.data.jabatan_id);
                    $("#nama").val(data.data.nama);
                    $("#rider_id").val(data.data.rider_id);
                    $("#no_wa").val(data.data.no_wa);
                    $("#password").val("Password hanya bisa di - Reset");
                    document.getElementById("password").readOnly = true;
                    $("#status").val(data.data.status);
                    $("#role").val(data.data.roles[0].name);
                    $(".modal-title").html('Form Ubah Rider / Pengguna');
                    $("#hubModal").modal('show');
                }
            });
        }

        function hapusHub(id){
            if(confirm('Apakah anda yakin hapus data ini ?') === true){
                $.ajax({
                    url: "{{route('rider.destroy')}}",
                    type: "POST",
                    dataType: 'json',
                    data: {
                        _token: '{{csrf_token()}}',
                        id: id
                    },
                    success: function (data){
                        if(data.code==='0'){
                            alert(data.msg);
                        }else{
                            location.reload();
                        }
                    }
                });
            }
        }

        function simpanHub(){
            $.ajax({
               url: "{{route('rider.store')}}",
               type: "POST",
               dataType: 'json',
               data: $("#hubForm").serialize(),
               success: function (data){
                   if(data.code==='0'){
                       closeModal();
                       alert(data.msg);
                   }else{
                       location.reload();
                   }
               }
            });
        }

        function simpanPass(){
            $.ajax({
                url: "{{route('rider.reset-password')}}",
                type: "POST",
                dataType: 'json',
                data: {
                    _token: '{{csrf_token()}}',
                    id_pass: $("#id_pass").val(),
                    reset_password: $("#reset_password").val(),
                },
                success: function (data){
                    if(data.code==='0'){
                        closePass();
                        alert(data.msg);
                    }else{
                        location.reload();
                    }
                }
            });
        }
    </script>
@endsection
