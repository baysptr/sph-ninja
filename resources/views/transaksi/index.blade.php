@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <h2>Daftar Transaksi Kurir / Rider</h2>
            <div class="form-group">
                <input type="date" class="form-control" onchange="showData(this)">
            </div>
        </div>
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <td colspan="2" class="text-center">RIDER</td>
                    <td colspan="2" class="text-center">KATEGORI PAKET</td>
                    <td rowspan="2" style="vertical-align: middle">POINT</td>
                    <td colspan="2" class="text-center">GAJI</td>
                    <td rowspan="2" style="vertical-align: middle">KOMISI<br/>SEWA<br/>MOTOR</td>
                    <td rowspan="2" style="vertical-align: middle">GAJI<br/>TOTAL</td>
                    <td colspan="2" rowspan="2" style="vertical-align: middle">
                        <button onclick="showModal()" class="btn btn-block btn-primary">Tambah Data!</button>
                    </td>
                </tr>
                <tr>
                    <td>NAMA</td>
                    <td>JABATAN</td>
                    <td>SMALL</td>
                    <td>BULKY</td>
                    <td>GAJI</td>
                    <td>JABATAN</td>
                </tr>
            </thead>
            <tbody id="tempat_data">
{{--                @foreach($transaksis as $transaksi)--}}
{{--                    <tr>--}}
{{--                        <td>{{$transaksi->rider->nama}}</td>--}}
{{--                        <td>{{$transaksi->rider->jabatan->nama_level}}</td>--}}
{{--                        <td>{{$transaksi->kuantiti[0]}}</td>--}}
{{--                        <td>{{$transaksi->kuantiti[1]}}</td>--}}
{{--                        <td>{{$transaksi->ocd}}</td>--}}
{{--                        <td>@currency($transaksi->gaji)</td>--}}
{{--                        <td>@currency($transaksi->gaji_kategori)</td>--}}
{{--                        <td>@currency($transaksi->komisi_sewa)</td>--}}
{{--                        <td>@currency($transaksi->gaji_total)</td>--}}
{{--                        <td><a href="#" onclick="editHub('{{$transaksi->id}}')" class="btn btn-block btn-info"><i class="fa fa-edit"></i></a></td>--}}
{{--                        <td><a href="#" onclick="hapusHub('{{$transaksi->id}}')" class="btn btn-block btn-warning"><i class="fa fa-trash"></i></a></td>--}}
{{--                    </tr>--}}
{{--                @endforeach--}}
            </tbody>
        </table>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="hubModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" onclick="closeModal()" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form name="hubFrom" id="hubForm">
                        @csrf
                        <input type="hidden" name="id" id="id">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Tanggal Transaksi Sukses</label>
                                    <input type="date" name="tanggal" id="tanggal" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Kurir / Rider</label>
                                    <select name="rider" id="rider" class="form-control">
                                        <option selected disabled>-- Pilih Rider / Kurir --</option>
                                        @foreach($riders as $rider)
                                            <option value="{{$rider->id}}">{{$rider->nama}} ({{$rider->rider_id}})</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        @foreach($kategoris as $kategori)
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Kategori Paket</label>
                                        <input type="hidden" value="{{$kategori->id}}" name="id_kategori_{{$kategori->id}}" id="id_kategori_{{$kategori->kategori}}" class="form-control">
                                        <input type="text" id="txt_kategori_{{$kategori->id}}" value="{{$kategori->kategori}}" class="form-control" readonly>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Kelompok Paket</label>
                                        <input type="text" id="paket" class="form-control" readonly value="@foreach($kategori->paket as $paket) {{$paket->nama_paket}} @endforeach">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Kuantiti</label>
                                        <input type="number" name="kuantiti_{{$kategori->id}}" id="kuantiti_{{$kategori->id}}" class="form-control">
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </form>
                </div>
                <div class="modal-footer">
                    <small>Transaksi ini dilakukan oleh: <strong>{{Auth::user()->nama}}</strong></small>
                    <button type="button" class="btn btn-secondary" onclick="closeModal()">Close</button>
                    <button type="button" class="btn btn-primary" onclick="simpanHub()">Simpan!</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer-script')
    <script>
        function showData(data){
            $.ajax({
                url: "{{route('transaksi.data')}}",
                type: "POST",
                dataType: 'json',
                data: {
                    _token: '{{csrf_token()}}',
                    tanggal: data.value
                },
                success: function (data){
                    $("#tempat_data").html(data.data);
                }
            });
        }

        function showModal(){
            $(".modal-title").html('Form Tambah Transaksi');
            $("#hubModal").modal('show');
        }

        function closeModal(){
            $("#id").val("");
            $("#tanggal").val("");
            $("#rider").val("");
            $("#kuantiti_1").val("");
            $("#kuantiti_2").val("");
            $("#hubModal").modal('hide');
        }

        function editHub(id){
            $.ajax({
                url: "/transaksi/show/"+id,
                type: "GET",
                dataType: 'json',
                success: function (data){
                    $("#id").val(data.data.id);
                    $("#tanggal").val(data.data.tanggal);
                    $("#rider").val(data.data.rider_id);
                    $("#kuantiti_1").val(data.data.kuantiti[0]);
                    $("#kuantiti_2").val(data.data.kuantiti[1]);
                    $(".modal-title").html('Form Ubah Paket');
                    $("#hubModal").modal('show');
                }
            });
        }

        function hapusHub(id){
            if(confirm('Apakah anda yakin hapus data ini ?') === true){
                $.ajax({
                    url: "{{route('transaksi.destroy')}}",
                    type: "POST",
                    dataType: 'json',
                    data: {
                        _token: '{{csrf_token()}}',
                        id: id
                    },
                    success: function (data){
                        if(data.code==='0'){
                            alert(data.msg);
                        }else{
                            location.reload();
                        }
                    }
                });
            }
        }

        function simpanHub(){
            $.ajax({
               url: "{{route('transaksi.store')}}",
               type: "POST",
               dataType: 'json',
               data: $("#hubForm").serialize(),
               success: function (data){
                   console.log(data);
                   if(data.code===200){
                       location.reload();
                   }else{
                       closeModal();
                       alert(data.message)
                   }
               }
            });
        }
    </script>
@endsection
