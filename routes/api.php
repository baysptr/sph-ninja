<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::group(['prefix' => 'kategori-hub'], function () {
    Route::get('/', 'KategoriHubController@index');
    Route::post('/store', 'KategoriHubController@store');
    Route::get('/{id}/show', 'KategoriHubController@show');
    Route::post('/update/{id}', 'KategoriHubController@update');
    Route::get('/delete/{id}', 'KategoriHubController@destroy');
});

Route::group(['prefix' => 'level-jabatan'], function () {
    Route::get('/', 'LevelJabatanController@index');
    Route::post('/store', 'LevelJabatanController@store');
    Route::get('/{id}/show', 'LevelJabatanController@show');
    Route::post('/update/{id}', 'LevelJabatanController@update');
    Route::get('/delete/{id}', 'LevelJabatanController@destroy');
});

Route::group(['prefix' => 'transaksi'], function () {
    Route::get('/', 'TransaksiController@index');
    Route::post('/store', 'TransaksiController@store');
    Route::get('/{id}/show', 'TransaksiController@show');
    Route::post('/update/{id}', 'TransaksiController@update');
    Route::get('/delete/{id}', 'TransaksiController@destroy');
});

