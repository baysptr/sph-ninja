<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('login'));
});

Route::get('/wa', 'TestController@wa');

Auth::routes([
    'login'    => true,
    'logout'   => true,
    'register' => false,
    'reset'    => false,   // for resetting passwords
    'confirm'  => false,  // for additional password confirmations
    'verify'   => false,  // for email verification
]);

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['role:ADMIN']], function () {
    Route::group(['prefix' => 'hub'], function () {
        Route::get('/', 'HubController@index')->name('hub.index');
        Route::get('/show/{id}', 'HubController@show')->name('hub.show');
        Route::post('/store', 'HubController@store')->name('hub.store');
        Route::post('/destroy', 'HubController@destroy')->name('hub.destroy');
    });

    Route::group(['prefix' => 'level-jabatan'], function () {
        Route::get('/', 'LevelJabatanController@create')->name('level-jabatan.index');
        Route::get('/show/{id}', 'LevelJabatanController@show')->name('level-jabatan.show');
        Route::post('/store', 'LevelJabatanController@store')->name('level-jabatan.store');
        Route::post('/destroy', 'LevelJabatanController@delete')->name('level-jabatan.destroy');
    });

    Route::group(['prefix' => 'jenis-paket'], function () {
        Route::get('/', 'KategoriPaketController@index')->name('jenis-paket.index');
        Route::get('/show/{id}', 'KategoriPaketController@show')->name('jenis-paket.show');
        Route::post('/store', 'KategoriPaketController@store')->name('jenis-paket.store');
        Route::post('/destroy', 'KategoriPaketController@destroy')->name('jenis-paket.destroy');
    });

    Route::group(['prefix' => 'paket'], function () {
        Route::get('/', 'PaketController@index')->name('paket.index');
        Route::get('/show/{id}', 'PaketController@show')->name('paket.show');
        Route::post('/store', 'PaketController@store')->name('paket.store');
        Route::post('/destroy', 'PaketController@destroy')->name('paket.destroy');
    });
});

Route::group(['middleware' => ['role:ADMIN|STAFF']], function () {
    Route::get('/dashboard/{month}', 'DashboardController@show')->name('dashboard.show');
    Route::get('/dashboard/pie/{month}', 'DashboardController@pie')->name('dashboard.pie.show');

    Route::group(['prefix' => 'rider'], function () {
        Route::get('/', 'RiderController@index')->name('rider.index');
        Route::get('/show/{id}', 'RiderController@show')->name('rider.show');
        Route::post('/store', 'RiderController@store')->name('rider.store');
        Route::post('/reset-password', 'RiderController@reset')->name('rider.reset-password');
        Route::post('/destroy', 'RiderController@destroy')->name('rider.destroy');
    });

    Route::group(['prefix' => 'transaksi'], function () {
        Route::get('/', 'TransaksiController@index')->name('transaksi.index');
        Route::get('/show/{id}', 'TransaksiController@show')->name('transaksi.show');
        Route::post('/data', 'TransaksiController@data')->name('transaksi.data');
        Route::post('/store', 'TransaksiController@store')->name('transaksi.store');
        Route::post('/destroy', 'TransaksiController@destroy')->name('transaksi.destroy');
    });
});

Route::group(['middleware' => ['role:RIDER']], function () {
    Route::get('/dashboard/rider/{month}', 'DashboardController@show_rider')->name('dashboard.show_rider');
    Route::get('/dashboard/rider/pie/{month}', 'DashboardController@pie_rider')->name('dashboard.pie.show_rider');
});
